import { getCurrentWeather, CurrentConditions } from './envCanada';

// mocking the axios dependency
import axios from 'axios';
jest.mock('axios');
const axiosMocked = axios as jest.Mocked<typeof axios>;

// mocking the xml2js dependency
import xml2js from 'xml2js';
jest.mock('xml2js');
const xml2jsMocked = xml2js as jest.Mocked<typeof xml2js>;

// Disabling estlint unbound method check
/* eslint-disable @typescript-eslint/unbound-method */

describe('Env Canada', () => {
  test('calls env canada api', async () => {
    // Tests if it is being called with the correct url
    expect.assertions(3);
    // get is the method returned to the module
    // the test is gonna feil because it will look for the field feed and feed will not be present on the parameter we are passing to
    //axiosMocked.get.mockResolvedValueOnce({ data: '' }); //--> don't needed because it will return undefined without it
    await expect(getCurrentWeather()).rejects.toThrow();
    expect(axiosMocked.get).toHaveBeenCalledTimes(1);
    expect(axiosMocked.get).toHaveBeenCalledWith(
      'https://weather.gc.ca/rss/city/bc-74_e.xml'
    );
  });
  test('gives data to xml parser', async () => {
    // Tests if the payload is being given to the xml parser
    expect.assertions(3);

    axiosMocked.get.mockResolvedValueOnce({ data: 'foo' });
    //xml2jsMocked.parseStringPromise.mockResolvedValueOnce(null); //--> don't needed because it will return undefined without it
    await expect(getCurrentWeather()).rejects.toThrow();
    expect(xml2jsMocked.parseStringPromise).toHaveBeenCalledTimes(1);
    expect(xml2jsMocked.parseStringPromise).toHaveBeenCalledWith('foo');
  });

  test('extracts current condition', async () => {
    // Tests if the current condition is correct extracted
    expect.assertions(2);

    axiosMocked.get.mockResolvedValueOnce({ data: '' });
    xml2jsMocked.parseStringPromise.mockResolvedValueOnce({
      // telling to mock return this specific object to test the extraction
      feed: {
        entry: [
          { title: '' },
          { title: ['Current Conditions: Mainly Sunny, 6.5°C'] },
        ],
      },
    });
    const actual: CurrentConditions = await getCurrentWeather();
    expect(actual.condition).toBe('Mainly Sunny');
    expect(actual.temp).toBe(6.5);
  });

  test('unparsable data', async () => {
    // Tests the error throw
    expect.assertions(1);

    axiosMocked.get.mockResolvedValueOnce({ data: '' });
    xml2jsMocked.parseStringPromise.mockResolvedValueOnce({
      feed: {
        entry: [{ title: '' }, { title: ['bad weather data'] }],
      },
    });
    await expect(getCurrentWeather()).rejects.toThrowError(/bad weather data$/); //regex used to state that the error string have to end with this value
  });
});
