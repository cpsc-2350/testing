import axios from 'axios';
import xml2js from 'xml2js';

const url = 'https://weather.gc.ca/rss/city/bc-74_e.xml';

interface EnvCanadaDataEntry {
  title: string[];
  updated: string[];
}

interface EnvCanadaData {
  feed: { entry: EnvCanadaDataEntry[] };
}

export interface CurrentConditions {
  condition: string;
  temp: number;
}

const conditionsRE = /Current Conditions: (?<condition>.+), (?<temp>[.0-9]+).C/;

export async function getCurrentWeather(): Promise<CurrentConditions> {
  const resp = await axios.get(url);
  const parsed = (await xml2js.parseStringPromise(resp.data)) as EnvCanadaData;
  const entry = parsed.feed.entry[1];
  const title = entry.title[0];
  // Current Conditions: Mainly Sunny, 6.5°C
  const matches = conditionsRE.exec(title);
  if (!!matches && !!matches.groups) {
    return {
      condition: matches.groups.condition,
      temp: +matches.groups.temp,
    };
  } else {
    throw Error("Couldn't parse conditions from:" + title);
  }
}
