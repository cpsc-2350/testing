export default function sum(...nums: number[]): number {
  return nums.reduce((prev, cur) => (prev += cur), 0);
}
