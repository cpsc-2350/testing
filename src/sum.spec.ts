import sum from './sum';
describe('sum', () => {
  ([
    ['empty', [], 0],
    ['l +ve', [4], 4],
    ['l -ve', [-4], -4],
    ['l +ve', [1, -2, 3], 2],
    //['bad', [-1, -2], 3], //-> bad case example (error)
  ] as [string, number[], number][]).forEach(([name, input, expected]) => {
    //casting in typescript
    //test(name, () => {    -> the first parameter is a string and you can manipulate it for whatever you need
    test(`${name} ${input.toString()}`, () => {
      expect(sum(...input)).toBe(expected);
    });
  });
});
/* 
  test('empty', () => {
    expect(sum()).toBe(0);
  });
  test('single +ve', () => {
    expect(sum(5)).toBe(5);
  });
  test('single -ve', () => {
    expect(sum(-5)).toBe(-5);
  });
  test('3 +/- ', () => {
    expect(sum(1, -2, 3)).toBe(2);
  });
*/
